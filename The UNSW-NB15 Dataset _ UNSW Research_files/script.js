(function ($, Drupal, window, document) {

  Drupal.behaviors.basic = {
    attach: function (context, settings) {

      /**
       * mmenu
       */
      if (!$('.mm-navbar .mm-close').length) {
        var close_btn = $('<a class="mm-close mm-btn"><span class="mm-sronly">Close menu</span></a>');
        close_btn.click(function(){
          $('#mm-blocker').trigger('mousedown');
        });
        $('.mm-navbar').append(close_btn);
      }

      /**
       * Image credit toggle
       */
      $('.banner-credit-toggle', context).on('click', function(){
        $(this).next().slideToggle();
      });


      /**
      * Profile edit accordion
      */
      if ($('#profile-tabs').length) {
        $('#profile-tabs', context).tabs();
        $('#profile-tabs h3.heading', context).click(function(){
          if($(this).hasClass('active')) { return; }
          $(this).siblings('.heading').removeClass('active');
          $(this).addClass('active');
          $(this).siblings('.content').slideUp();
          $(this).next().slideDown();
        });
        $('#profile-tabs #block-block-12 h3', context).click(function(){
          $(this).parent().toggleClass('active');
        });
      }


      /**
       * Profile enquiry
       */
      $('#block-colorcodesexplainprofile h2', context).on('click', function(){
        $(this).parent().toggleClass('active');
      });

      $('.enquiry_hr', context).on('click', function() {
          setupEnquiryForm();
      });
      $('.enquiry_keyword', context).on('click', function() {
          setupKeywordForm();
      });

      /**
      * Toggle lock/unlock profile fields
      */
     $('#edit-field-override-title-value', context).click(function(){
        if($(this).is(':checked')){
          $('#edit-field-title-0-value').removeAttr('disabled');
        } else {
          $('#edit-field-title-0-value').attr('disabled', 'disabled');
        }
      });

      $('#edit-field-override-faculty-value', context).click(function(){
        if($(this).is(':checked')){
          $('#edit-field-faculty-wrapper input.form-autocomplete').removeAttr('disabled');
        } else {
          $('#edit-field-faculty-wrapper input.form-autocomplete').attr('disabled', 'disabled');
        }
      });

      $('#edit-field-override-school-value', context).click(function(){
        if($(this).is(':checked')){
          $('#edit-field-school-0-target-id').removeAttr('disabled');
        } else {
          $('#edit-field-school-0-target-id').attr('disabled', 'disabled');
        }
      });

      $('#edit-field-override-university-role-value', context).click(function(){
        if($(this).is(':checked')){
          $('#edit-field-university-role-0-value').removeAttr('disabled');
        } else {
          $('#edit-field-university-role-0-value').attr('disabled', 'disabled');
        }
      });


      // Alert when moving away from edit page.
      var dirty = false;
      $(document).ready(function(){

        $("#profile-general-edit-form, #profile-fass-edit-form, #profile-fbe-edit-form").submit(function(){
          dirty = false;
        });

        setTimeout(function() {
          var current = $("iframe").contents().find('html');
          var timer;
          current.bind({ keyup: function(){
            clearTimeout(timer);
            timer = setTimeout(function() {dirty = true; console.log('iframe');}, 1000);
          }});

        }, 1000);

        $('.node-form input, .research-project-form input, .profile-form input, .node-form textarea, .node-form select, .node-form iframe').change(function(){
         // dirty = true;
        });
      });

      window.onbeforeunload = function(){
        if(dirty) {
          return "Save the page or your changes will be lost!";
        }
      };

      $(document).ready(function(){
        setTimeout(function(){
          adjustQuickFactHeight();
          adjustPubsSummaryHeight();
        },300);
      });


      // fact-value countto
      $('.fact-value', context).each(function(){
        //$(this).countTo({from: 0, to: 500});
        var text = $(this).text();
        var num = text.match(/([0-9,\.]+)/g);
        $(this).html(text.replace(num,'<span>' + num + '</span>'));

        if (num % 1 ===0) {
          $(this).find('span').countTo({from: 0, to: num});
        } else {
          $(this).find('span').countTo({from: 0, to: num, formatter: function (value, options) {
            return value.toFixed(1);
          }});
        }
      });


      // video collection
      $('.video-thumb', context).on('click', function(){
          var index = $(this).index();
          $(this).siblings().removeClass('active');
          $(this).addClass('active');
          $(this).closest('.field-video-collection').find('.video-player > div').addClass('hide');
          $(this).closest('.field-video-collection').find('.video-player > div:nth-child(' + (index+1) + ')').removeClass('hide');
      });

      var youtube = document.querySelectorAll( ".video-player .youtube" );
      for (var i = 0; i < youtube.length; i++) {
          // thumbnail image source.
          var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
          // Load the image asynchronously
          var image = new Image();
              image.src = source;
              image.addEventListener( "load", function() {
                  youtube[ i ].appendChild( image );
              }( i ) );

          youtube[i].addEventListener( "click", function() {
              var iframe = document.createElement( "iframe" );

              iframe.setAttribute( "frameborder", "0" );
              iframe.setAttribute( "allowfullscreen", "" );
              iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );

              this.innerHTML = "";
              this.appendChild( iframe );
          } );
      }

      // profile bio
      if ($('.profile-view .body-summary').length) {
        $('.profile-view .body-summary').show();
        $('.profile-view .body-full').hide();
        $('.body-toggle', context).click(function(e){
          e.preventDefault();
          if ($(this).closest('.body-full').length) {
            $('.profile-view .body-summary').show();
            $('.profile-view .body-full').slideUp();
          } else {
            $('.profile-view .body-summary').hide();
            $('.profile-view .body-full').slideDown();
          }
        });
      }

      // for every slide in activities carousel, copy the next slide's item in the slide.
      // Do the same for the next, next item.
      if ($(window).width() >= 768) {
        $('#carousel-activities .item', context).each(function(){
          var next = $(this).next();
          if (!next.length) {
            next = $(this).siblings(':first');
          }
          next.children(':first-child').clone().appendTo($(this));

          if (next.next().length>0) {
            next.next().children(':first-child').clone().appendTo($(this));
          } else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
          }
        });
        $('#carousel-activities', context).on('slid.bs.carousel', '', function() {
          var $this = $(this);

          $this.children('.carousel-control').removeClass('hide');

          if($('.carousel-inner .item:lt(-3)').hasClass('active')) {
            $this.children('.left.carousel-control').addClass('hide');
          } else if($('.carousel-inner .item:lt(3)').hasClass('active')) {
            $this.children('.right.carousel-control').addClass('hide');
          }
        });
      } else {
        $('#carousel-activities', context).on('slid.bs.carousel', '', function() {
          var $this = $(this);

          $this.children('.carousel-control').removeClass('hide');

          if($('.carousel-inner .item:first-child').hasClass('active')) {
            $this.children('.left.carousel-control').addClass('hide');
          } else if($('.carousel-inner .item:last-child').hasClass('active')) {
            $this.children('.right.carousel-control').addClass('hide');
          }
        });
      }

      // sections toggle
      $('.field-sections .field-title', context).each(function(){
        if (!$(this).find('button').length) {
          $(this).append('<button><span class="fa fa-chevron-down"></span></button>');
        }
      });
      $('.field-sections .field-is-expanded', context).each(function(){
        if ($(this).text() == '1') {
          $(this).parent().find('.field-section-body').css('display','block');
          $(this).parent().find('.field-title span').removeClass('fa-chevron-down');
          $(this).parent().find('.field-title span').addClass('fa-chevron-up');
        }
      });
      //$('.field-sections .field-title', context).unbind('click');
      $('.field-sections .field-title', context).bind('click', function(){
        $(this).parent().find('.field-section-body').slideToggle();
        $(this).parent().find('.field-title span').toggleClass('fa-chevron-up');
        $(this).parent().find('.field-title span').toggleClass('fa-chevron-down');
      });


      $(document).ready(function(){
        $('.flexslider:not(.processed)').each(function(){
          var slider = $(this);
          slider.on('click', '.flex-control-thumbs', function() {
            var pos = $(this).parents('.flexslider').offset().top;
            var current = $(document).scrollTop();
            if (current > pos) {
              $([document.documentElement, document.body]).animate({
                  scrollTop: $(this).parents('.flexslider').offset().top
              }, 300);
            }
          });
          slider.flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            smoothHeight: 1
          });
          if (!slider.hasClass('start')) {
            slider.flexslider("stop");
          }
          slider.addClass('processed');

          // add caption to slider thumbnail
          setTimeout(function() {slider.find('.flex-control-nav li').each(function(){
            var index = $(this).index()+1;
            var caption = slider.find('.flex-viewport > ul > li:eq('+ index +') .caption');
            if (caption) {
              $(this).find('img').attr('title', caption.text().trim());
              $(this).find('img').tooltip();
            }
          });}, 2000);
        });
     });


      if ($('.showcase').length) {
        $.stellar({
          horizontalScrolling: false
        });
      }

      $(window).resize(function () {
        // Execute code when the window is resized.
        adjustQuickFactHeight();
        adjustPubsSummaryHeight();
      });

      $(window).scroll(function () {
        // Execute code when the window scrolls.
      });

      $(document).ready(function () {
        // Execute code once the DOM is ready.
        if ($('img[usemap]').length) {
          $('img[usemap]').rwdImageMaps();
        }

        // read more
        $('a.read-more', context).click(function(){
          if ($(this).parent().prev().hasClass('hide')) {
            $(this).parent().prev().removeClass('hide');
            $(this).text('Read less');
          } else {
            $(this).parent().prev().addClass('hide');
            $(this).text('Read more');
          }
        });
              
      

      });
    }

  };

  function adjustQuickFactHeight() {
    if ($('.quickfacts').length && !$('.quickfacts').parents('.page-node-type-homepage').length) {
        if ($(window).width() < 992) {
            $('#quick-facts .block').css('height', 'auto');
            $('.quickfacts').css('top', '');
        } else {
            $('#quick-facts .block').height($('main').height() - 60);


            var main_pos = $('main').offset();
            var fact_pos = $('#quick-facts .block').offset();
            var top = (main_pos.top - fact_pos.top);
            if (top) {
              $('.quickfacts').css('top', top + 'px');
            }
        }
    }
  }

  function adjustPubsSummaryHeight() {
    if ($('#pubs-summary').length) {
        if ($(window).width() > 768) {
            $('.profile-details').css('min-height', $('.rhs').height()-30);
        } else {
            $('.profile-details').css('min-height','');
        }
    }
  }

  function setupEnquiryForm() {
      $('body #hr_enquiry').remove();

      // generate modal
      var m = $('<div class="modal" id="hr_enquiry"></div>');
      var d = $('<div class="modal-dialog"></div>');
      var c = $('<div class="modal-content"></div>');
      var b = $('<div class="modal-body"><h2>My Personal data correction: HR</h2></div>');
      var f = $('<div class="modal-footer"></div>');
      var name = $('<div class="form-group"><label>Your name*</label><br /><input type="text" class="name form-control"></input></div>');
      var email = $('<div class="form-group"><label>Your email*</label><br /><input type="email" class="email form-control"></input></div>');
      var message = $('<div class="form-group"><label>Message*</label><br /><textarea class="message form-control">I believe the data in the field/s below is incorrect.\n\nName,\nUNSW Employer ID,\nTitle,\nFirst name,\nLast name,\nUniversity role,\nSchool,\nFaculty\n\nIt shoud state...</textarea><p>Please, leave only fields with incorrect data and provide correct version with comments;</p></div>');
      var cancel = $('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
      var send = $('<button type="button" class="btn btn-primary">Send</button>');
      b.append(name);
      b.append(email);
      b.append(message);
      send.on('click',function(){submitEnquiryForm(name,email,message);});
      f.append(cancel);
      f.append(send);
      c.append(b);
      c.append(f);
      d.append(c);
      m.append(d);
      $('body').append(m);
      $('#hr_enquiry').modal();
  }

  function submitEnquiryForm(name,email,message) {
    var error = false;
    name.find('.text-danger').remove();
    name.removeClass('has-error');
    if (name.find('.name').val() == '') {
      error = true;
      name.append('<span class="text-danger">Name is required</span>');
      name.addClass('has-error');
    }

    email.find('.text-danger').remove();
    email.removeClass('has-error');
    if (email.find('.email').val() == '') {
      error = true;
      email.append('<span class="text-danger">Email is required</span>');
      email.addClass('has-error');
    } else if (!/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email.find('.email').val())) {
      error = true;
      email.append('<span class="text-danger">Email is invalid</span>');
      email.addClass('has-error');
    }

    message.find('.text-danger').remove();
    message.removeClass('has-error');
    if (message.find('.message').val() == '') {
      error = true;
      message.append('<span class="text-danger">Message is required</span>');
      message.addClass('has-error');
    }

    var staffid = $('#block-resgate8-page-title h1').text().replace('Edit Profile - ','');

    if (!error) {
      $.ajax('/hr-enquiry',{
        data: {
          name: name.find('.name').val(),
          email: email.find('.email').val(),
          message: message.find('.message').val(),
          staffid: staffid,
          type: 'hr'
        }
      }).done(function(d){
        if(d=='success'){
          $('#hr_enquiry .modal-body').html('Your change request have been sent.');
        } else {
          $('#hr_enquiry .modal-body').html('<span class="text-danger">Error sending message. Please try again later.</span>');
        }
        $('#hr_enquiry .btn-primary').remove();
      });
    }

  }

  function setupKeywordForm() {
      $('body #keyword_enquiry').remove();

      // generate modal
      var m = $('<div class="modal" id="keyword_enquiry"></div>');
      var d = $('<div class="modal-dialog"></div>');
      var c = $('<div class="modal-content"></div>');
      var b = $('<div class="modal-body"><h2>Tell us what keywords do you want to add</h2></div>');
      var f = $('<div class="modal-footer"></div>');
      var name = $('<div class="form-group"><label>Your name*</label><br /><input type="text" class="name form-control"></input></div>');
      var email = $('<div class="form-group"><label>Your email*</label><br /><input type="email" class="email form-control"></input></div>');
      var message = $('<div class="form-group"><label>Message*</label><br /><textarea class="message form-control">Here is a list of keywords that I want to add:\n\nkeyword 1,\nkeyword 2,\nkeyword 3...</textarea><p>Please provide a list of keywords that you want to add.</p></div>');
      var cancel = $('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
      var send = $('<button type="button" class="btn btn-primary">Send</button>');
      b.append(name);
      b.append(email);
      b.append(message);
      send.on('click',function(){submitKeywordForm(name,email,message);});
      f.append(cancel);
      f.append(send);
      c.append(b);
      c.append(f);
      d.append(c);
      m.append(d);
      $('body').append(m);
      $('#keyword_enquiry').modal();
  }

  function submitKeywordForm(name,email,message) {
    var error = false;
    name.find('.text-danger').remove();
    name.removeClass('has-error');
    if (name.find('.name').val() == '') {
      error = true;
      name.append('<span class="text-danger">Name is required</span>');
      name.addClass('has-error');
    }

    email.find('.text-danger').remove();
    email.removeClass('has-error');
    if (email.find('.email').val() == '') {
      error = true;
      email.append('<span class="text-danger">Email is required</span>');
      email.addClass('has-error');
    } else if (!/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email.find('.email').val())) {
      error = true;
      email.append('<span class="text-danger">Email is invalid</span>');
      email.addClass('has-error');
    }

    message.find('.text-danger').remove();
    message.removeClass('has-error');
    if (message.find('.message').val() == '') {
      error = true;
      message.append('<span class="text-danger">Message is required</span>');
      message.addClass('has-error');
    }

    var staffid = $('#block-resgate8-page-title h1').text().replace('Edit Profile - ','');
    var pathArray = window.location.pathname.split( '/' );

    if (!error) {
      $.ajax('/hr-enquiry',{
        data: {
          name: name.find('.name').val(),
          email: email.find('.email').val(),
          message: message.find('.message').val(),
          staffid: staffid,
          type: 'keyword',
          userid: pathArray.length > 1 ? pathArray[2] : null
        }
      }).done(function(d){
        if(d=='success'){
          $('#keyword_enquiry .modal-body').html('Your change request have been sent.');
        } else {
          $('#keyword_enquiry .modal-body').html('<span class="text-danger">Error sending message. Please try again later.</span>');
        }
        $('#keyword_enquiry .btn-primary').remove();
      });
    }

  }

} (jQuery, Drupal, this, this.document));
